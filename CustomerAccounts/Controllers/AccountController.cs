﻿using CustomerAccounts.Business.Contracts;
using CustomerAccounts.Client.Entities;
using Microsoft.AspNetCore.Mvc;
using System;

namespace CustomerAccounts.Controllers
{
    public class AccountController : BaseApiController
    {
        private IAccountService _accountService;
        public AccountController(IAccountService accountService)
        {
            _accountService = accountService;
        }

        [HttpGet("AddAccount")]
        public ActionResult<ClientResponse> Create(Guid UserId, double InitialCredit)
        {
            return ExecuteOperation(() =>
            {
                return _accountService.AddAccount(UserId, InitialCredit);
            });
        }
    }
}
