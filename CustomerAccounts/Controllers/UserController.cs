﻿using CustomerAccounts.Business.Contracts;
using CustomerAccounts.Business.Entities;
using CustomerAccounts.Client.Entities;
using Microsoft.AspNetCore.Mvc;
using System;

namespace CustomerAccounts.Controllers
{
    public class UserController : BaseApiController
    {
        private IUserService _userService;
        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpPost("Create")]
        public ActionResult<ClientResponse> Create(UserRequest userRequest)
        {
            return ExecuteOperation(() =>
            {
                return _userService.AddUser(userRequest);
            });
        }

        [HttpGet("Users")]
        public ActionResult<ClientResponse> Get()
        {
            return ExecuteOperation(() =>
            {
                return _userService.GetUsers();
            });
        }

        [HttpGet("UserById")]
        public ActionResult<ClientResponse> Get(Guid UserId)
        {
            return ExecuteOperation(() =>
            {
                return _userService.UserById(UserId);
            });
        }
    }
}
