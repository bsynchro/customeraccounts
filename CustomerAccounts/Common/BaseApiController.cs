﻿using CustomerAccounts.Client.Entities;
using CustomerAccounts.Common.Exceptions;
using CustomerAccounts.Common.Extensions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Net;

namespace CustomerAccounts.Controllers
{
    public class BaseApiController : ControllerBase
    {
        private HttpContext incommingContext;
        protected HttpContext IncommingContext
        {
            get
            {
                if (HttpContext != null)
                    incommingContext = HttpContext;

                return incommingContext;
            }
        }
        public BaseApiController()
        {
            this.incommingContext = HttpContext;
        }


        protected ActionResult<ClientResponse> ExecuteOperation(Func<ClientResponse> codetoExecute)
        {
            try
            {
                return codetoExecute.Invoke();
            }
            catch (NotValidException ex)
            {
                SetStatusCode(HttpStatusCode.BadRequest);
                return ex.ClientResponse;
            }
            catch (NotAuthorizedException ex)
            {
                SetStatusCode(HttpStatusCode.Unauthorized);
                return ex.ClientResponse;
            }
            catch (NotLicensedException ex)
            {
                SetStatusCode(HttpStatusCode.Unauthorized);
                return ex.ClientResponse;
            }
            catch (NotModifiedException ex)
            {
                SetStatusCode(HttpStatusCode.NotModified);
                SetRevision(ex.Message);
                return ex.ClientResponse;
            }
            catch (NotFoundException ex)
            {
                SetStatusCode(HttpStatusCode.NotFound);
                return ex.ClientResponse;
            }
            catch (IntegrationException ex)
            {
                //logAdapter.WriteLog(ex);
                SetStatusCode(HttpStatusCode.BadRequest);
                return ex.ClientResponse;
            }
            catch (AlreadyExistException ex)
            {
                //logAdapter.WriteLog(ex);
                SetStatusCode(HttpStatusCode.Conflict);
                return ex.ClientResponse;
            }
            catch (InactiveException ex)
            {
                //logAdapter.WriteLog(ex);
                SetStatusCode(HttpStatusCode.BadRequest);
                return ex.ClientResponse;
            }
            catch (NotVerifiedException ex)
            {
                //logAdapter.WriteLog(ex);
                SetStatusCode(HttpStatusCode.BadRequest);
                return ex.ClientResponse;
            }
            catch (ExpiredException ex)
            {
                //logAdapter.WriteLog(ex);
                SetStatusCode(HttpStatusCode.BadRequest);
                return ex.ClientResponse;
            }
            catch (Exception ex)
            {
                //logAdapter.WriteLog(ex);
                SetStatusCode(HttpStatusCode.InternalServerError);
                return ex.ClientResponse();
            }
        }

        protected void SetRevision(string latestRevision)
        {
            IncommingContext.Response.ContentType = latestRevision;
        }

        protected void SetStatusCode(HttpStatusCode code)
        {
            IncommingContext.Response.StatusCode = (int)code;
        }
    }
}
