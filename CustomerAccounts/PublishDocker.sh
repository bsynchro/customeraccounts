echo "MO --- STARTING"

#cd "MOSmppServer/"
docker rmi fadelle/bsynchro-customeraccounts:latest
rm -frd publish/
dotnet publish CustomerAccounts.csproj -c Release -o publish
docker build -t fadelle/bsynchro-customeraccounts:latest .
docker push fadelle/bsynchro-customeraccounts:latest
rm -frd publish/
cd ..
cd ..

echo "MO --- FINISHED"

sleep 1d
