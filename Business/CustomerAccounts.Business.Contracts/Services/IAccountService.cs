﻿using CustomerAccounts.Client.Entities;
using System;

namespace CustomerAccounts.Business.Contracts
{
    public interface IAccountService
    {
        ClientResponse AddAccount(Guid CustomerId, double InitialCredit);
    }
}
