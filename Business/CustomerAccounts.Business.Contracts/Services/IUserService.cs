﻿using CustomerAccounts.Business.Entities;
using CustomerAccounts.Client.Entities;
using System;

namespace CustomerAccounts.Business.Contracts
{
    public interface IUserService
    {
        ClientResponse AddUser(UserRequest userRequest);
        ClientResponse GetUsers();
        ClientResponse UserById(Guid CustomerId);
    }
}
