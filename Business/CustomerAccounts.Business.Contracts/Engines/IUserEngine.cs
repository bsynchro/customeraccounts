﻿using CustomerAccounts.Business.Entities;
using System;
using System.Collections.Generic;

namespace CustomerAccounts.Business.Contracts
{
    public interface IUserEngine
    {
        bool AddUser(User user);
        List<User> GetUsers();
        User UserById(Guid CustomerId);
    }
}
