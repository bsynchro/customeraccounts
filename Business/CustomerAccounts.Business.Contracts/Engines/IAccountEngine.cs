﻿using CustomerAccounts.Business.Entities;
using System;

namespace CustomerAccounts.Business.Contracts
{
    public interface IAccountEngine
    {
        public Account Create(Guid UserId);

        public Account Create(Guid UserId, double InitialCredit);
    }
}
