﻿using AutoMapper;

namespace CustomerAccounts.Business.Contracts
{
    public interface IAutoMappers
    {
        IMapper Mapper { get; }
    }
}
