﻿using System;

namespace CustomerAccounts.Business.Entities
{
    public class Transaction
    {
        public Guid TransactionId { get; set; }
        public Guid AccountId { get; set; }
        public double Amount { get; set; }
        public DateTime ActionDate { get; set; }


        public Transaction Clone(Guid AccountId,double Amount)
        {
            return new Transaction
            {
                TransactionId = Guid.NewGuid(),
                AccountId = AccountId,
                Amount = Amount,
                ActionDate = DateTime.Now

            };
        }
    }
}
