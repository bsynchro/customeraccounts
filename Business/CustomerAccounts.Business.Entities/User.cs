﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace CustomerAccounts.Business.Entities
{
    public class User
    {
        public Guid UserId { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public List<Account> Accounts { get; set; }
    }
}
