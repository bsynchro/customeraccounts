﻿using System;
using System.Collections.Generic;

namespace CustomerAccounts.Business.Entities
{
    public class Account
    {
        public Guid AccountId { get; set; }
        public Guid CustomerId { get; set; }
        public double Balance { get; set; }
        public DateTime CreatedDate { get; set; }
        public List<Transaction> Transations { get; set; }


        public Account Clone(Guid CustomerId,double Balance = 0)
        {
            return new Account
            {
                AccountId = Guid.NewGuid(),
                CustomerId = CustomerId,
                Balance = Balance,
                Transations = new List<Transaction>()
            };
        }
    }
}
