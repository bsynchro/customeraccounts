﻿using CustomerAccounts.Business.Contracts;
using Microsoft.Extensions.DependencyInjection;

namespace CustomerAccounts.Business.Bootstrapper
{
    public static class StartupExtensions
    {
        public static IServiceCollection Init(this IServiceCollection services)
        {
            services.RegisterServices();
            return services;
        }

        private static IServiceCollection RegisterServices(this IServiceCollection services)
        {
            //Engines
            services.AddSingleton<IUserEngine, UserEngine>();
            services.AddSingleton<IAccountEngine, AccountEngine>();

            //Services
            services.AddSingleton<IUserService, UserService>();
            services.AddSingleton<IAccountService, AccountService>();

            //Adapters
            services.AddSingleton<IAutoMappers,AutoMappers> ();

            return services;
        }

    }
}
