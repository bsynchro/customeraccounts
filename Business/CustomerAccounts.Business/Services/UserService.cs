﻿using CustomerAccounts.Business.Contracts;
using CustomerAccounts.Business.Entities;
using CustomerAccounts.Client.Entities;
using System;

namespace CustomerAccounts.Business
{
    public class UserService : ClientResponseManager, IUserService
    {
        private readonly IUserEngine _userEngine;
        private IAutoMappers _autoMappers;
        public UserService(IUserEngine userEngine, IAutoMappers autoMappers)
        {
            _userEngine = userEngine;
            _autoMappers = autoMappers;
        }
        public ClientResponse AddUser(UserRequest userRequest)
        {
            User user = _autoMappers.Mapper.Map<UserRequest, User>(userRequest);
            _userEngine.AddUser(user);
            return Success();
        }

        public ClientResponse UserById(Guid UserId)
        {
            return Success(_userEngine.UserById(UserId));
        }

        public ClientResponse GetUsers()
        {
            return Success(_userEngine.GetUsers());
        }
    }
}
