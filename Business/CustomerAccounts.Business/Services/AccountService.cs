﻿using CustomerAccounts.Business.Contracts;
using CustomerAccounts.Business.Entities;
using CustomerAccounts.Client.Entities;
using CustomerAccounts.Common.Static;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CustomerAccounts.Business
{
    public class AccountService : ClientResponseManager, IAccountService
    {
        private IUserEngine _userEngine;
        private IAccountEngine _accountEngine;

        public AccountService(IUserEngine userEngine, IAccountEngine accountEngine)
        {
            _userEngine = userEngine;
            _accountEngine = accountEngine;
        }

        public ClientResponse AddAccount(Guid UserId, double InitialCredit)
        {
            List<User> users = _userEngine.GetUsers();
            User user = users.FirstOrDefault(x => x.UserId == UserId);

            if (user == null)
                return SuccessWithFailureStatus(Messages.UserNotFound);

            if (InitialCredit == 0)
                user.Accounts.Add(_accountEngine.Create(UserId));
            else
                user.Accounts.Add(_accountEngine.Create(UserId, InitialCredit));

            return Success();
        }
    }
}
