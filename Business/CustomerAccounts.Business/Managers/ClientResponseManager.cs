﻿using CustomerAccounts.Client.Entities;
using CustomerAccounts.Common.Static;

namespace CustomerAccounts.Business
{
    public class ClientResponseManager
    {
        #region Fields 
        public ClientResponseManager()
        {
        }

        #endregion

        #region Operations

        public static ClientResponse Success(object data)
        {
            return new ClientResponse()
            {
                ResponseCode = System.Net.HttpStatusCode.OK,
                Status = Messages.Success,
                Data = data
            };
        }

        public static ClientResponse Success()
        {
            return new ClientResponse()
            {
                ResponseCode = System.Net.HttpStatusCode.OK,
                Status = Messages.Success,
                Data = null
            };
        }

        public static ClientResponse Created()
        {
            return new ClientResponse()
            {
                ResponseCode = System.Net.HttpStatusCode.Created,
                Status = Messages.Success,
                Data = null
            };
        }

        public static ClientResponse Created(object data)
        {
            return new ClientResponse()
            {
                ResponseCode = System.Net.HttpStatusCode.Created,
                Status = Messages.Success,
                Data = data
            };
        }

        /// <summary>
        /// This is a specific case when we need to return a success response with failure status and message
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public ClientResponse SuccessWithFailureStatus(object data)
        {
            return new ClientResponse()
            {
                ResponseCode = System.Net.HttpStatusCode.OK,
                Status = Messages.Failure,
                Data = data
            };
        }

        #endregion

     
    }
}
