﻿using CustomerAccounts.Business.Contracts;
using CustomerAccounts.Business.Entities;
using System;

namespace CustomerAccounts.Business
{
    public class AccountEngine : IAccountEngine
    {
        public AccountEngine()
        {
        }

        public Account Create(Guid UserId)
        {
            return new Account().Clone(UserId);
        }

        public Account Create(Guid UserId, double InitialCredit)
        {
            Account account = new Account().Clone(UserId, InitialCredit);
            account.Transations.Add(new Transaction().Clone(account.AccountId,InitialCredit));
            return account;
        }
    }
}
