﻿using CustomerAccounts.Business.Contracts;
using CustomerAccounts.Business.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CustomerAccounts.Business
{
    public class UserEngine : IUserEngine
    {
        private List<User> users = null;

        public UserEngine()
        {
            users = new List<User>();
        }

        public bool AddUser(User user)
        {
            user.Accounts = new List<Account>();
            users.Add(user);
            return true;
        }

        public User UserById(Guid UserId)
        {
            return users.FirstOrDefault(user => user.UserId == UserId);
        }

        public List<User> GetUsers()
        {
            return users;
        }
    }
}
