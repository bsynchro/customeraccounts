﻿using AutoMapper;
using CustomerAccounts.Business.Contracts;
using CustomerAccounts.Business.Entities;
using CustomerAccounts.Client.Entities;

namespace CustomerAccounts.Business
{
    public class AutoMappers : IAutoMappers
    {
        public IMapper Mapper { get; private set; }
        public AutoMappers()
        {
            ConfigureMappers();
        }

        public void ConfigureMappers()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<UserRequest, User>().IgnoreAllPropertiesWithAnInaccessibleSetter();
            });

            Mapper = config.CreateMapper();
            //config.AssertConfigurationIsValid();
        }
    }
}
