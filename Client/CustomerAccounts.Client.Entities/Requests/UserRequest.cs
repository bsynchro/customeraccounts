﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace CustomerAccounts.Client.Entities
{
    public class UserRequest
    {
        public Guid UserId { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
    }
}
