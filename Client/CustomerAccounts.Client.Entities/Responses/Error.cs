﻿using Newtonsoft.Json;

namespace CustomerAccounts.Client.Entities
{
    public class Error
    {
        public Error()
        {

        }
        public Error(int code, string message, string title = null)
        {
            this.Title = title;
            this.Code = code;
            this.Message = message;
        }


        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("code")]
        public int Code { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }
    }
}
