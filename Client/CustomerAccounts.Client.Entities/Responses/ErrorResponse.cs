﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace CustomerAccounts.Client.Entities
{
    public class ErrorResponse
    {
        [JsonProperty("errors")]
        public List<Error> Errors { get; set; }

        public ErrorResponse(int code, string message)
        {
            Errors = new List<Error>()
            {
                new Error()
                {
                     Code = code,
                     Message = message
                }
            };
        }

        public ErrorResponse(int code, List<string> messages)
        {
            Errors = new List<Error>();
            foreach (var item in messages)
            {
                Errors.Add(new Error() { Code = code, Message = item });
            }
        }
    }
}
