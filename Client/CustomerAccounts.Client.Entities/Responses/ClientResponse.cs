﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net;

namespace CustomerAccounts.Client.Entities
{
    public class ClientResponse
    {
        public ClientResponse()
        {
            this.ResponseCode = HttpStatusCode.OK;
            this.Title = null;
        }
        public ClientResponse(string status, Error errorResponse, string title = null)
        {
            this.Title = title;
            this.ResponseCode = HttpStatusCode.OK;
            List<Error> errors = new List<Error>()
            {
                new Error()
                {
                     Code = errorResponse.Code,
                     Message = errorResponse.Message
                }
            };
            this.Status = status;
            this.Data = errors;
        }
        public ClientResponse(string status, Error errorResponse, HttpStatusCode statusCode, string title = null)
        {
            this.Title = title;
            this.ResponseCode = statusCode;
            List<Error> errors = new List<Error>()
            {
                new Error()
                {
                     Code = errorResponse.Code,
                     Message = errorResponse.Message
                }
            };
            this.Status = status;
            this.Data = errors;
        }


        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("result")]
        public object Data { get; set; }

        [JsonIgnore]
        public HttpStatusCode ResponseCode { get; set; }
    }

  
}
