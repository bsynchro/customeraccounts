﻿using CustomerAccounts.Client.Entities;
using System;

namespace CustomerAccounts.Common.Exceptions
{
    public class InternalErrorException : ApplicationException
    {
        public ClientResponse ClientResponse { get; set; }

        public InternalErrorException(string message, ClientResponse response)
            : base(message)
        {
            this.ClientResponse = response;
        }
        public InternalErrorException(string message, ClientResponse response, Exception ex)
            : base(message, ex)
        {
            this.ClientResponse = response;
        }


        public InternalErrorException(string message)
            : base(message)
        {
            this.ClientResponse = new ClientResponse()
            {

            };
        }

        public InternalErrorException(string message, Exception exception)
            : base(message, exception)
        {
        }
    }
}
