﻿using CustomerAccounts.Client.Entities;
using System;

namespace CustomerAccounts.Common.Exceptions
{
    public class NotVerifiedException : ApplicationException
    {
        public ClientResponse ClientResponse { get; set; }

        public NotVerifiedException(string message, ClientResponse response)
            : base(message)
        {
            this.ClientResponse = response;
        }

        public NotVerifiedException(string message, ClientResponse response, Exception ex)
            : base(message, ex)
        {
            this.ClientResponse = response;
        }


        public NotVerifiedException(string message)
            : base(message)
        {
        }


        public NotVerifiedException(string message, Exception exception)
            : base(message, exception)
        {
        }
    }
}
