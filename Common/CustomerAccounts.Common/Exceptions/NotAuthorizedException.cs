﻿using CustomerAccounts.Client.Entities;
using System;

namespace CustomerAccounts.Common.Exceptions
{

    public class NotAuthorizedException : ApplicationException
    {

        public ClientResponse ClientResponse { get; set; }

        public NotAuthorizedException(string message)
            : base(message)
        {
        }

        public NotAuthorizedException(string message, Exception ex)
            : base(message, ex)
        {
        }

        public NotAuthorizedException(string message, ClientResponse response)
            : base(message)
        {
            this.ClientResponse = response;

        }

        public NotAuthorizedException(string message, ClientResponse response, Exception ex)
            : base(message, ex)
        {
            this.ClientResponse = response;

        }

    }
}
