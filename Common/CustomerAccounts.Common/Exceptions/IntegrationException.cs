﻿using CustomerAccounts.Client.Entities;
using System;
using System.Net;

namespace CustomerAccounts.Common.Exceptions
{
    public class IntegrationException : ApplicationException
    {
        public HttpStatusCode StatusCode { get; set; }
        public ClientResponse ClientResponse { get; set; }

        public IntegrationException(string message, ClientResponse response)
            : base(message)
        {
            this.ClientResponse = response;
        }

        public IntegrationException(string message, ClientResponse response, Exception ex)
            : base(message, ex)
        {
            this.ClientResponse = response;
        }


        public IntegrationException(string message)
            : base(message)
        {
        }

        public IntegrationException(string message, Exception ex)
            : base(message, ex)
        {
        }

    }
}
