﻿using CustomerAccounts.Client.Entities;
using System;

namespace CustomerAccounts.Common.Exceptions
{

    public class NotLicensedException : ApplicationException
    {
        public ClientResponse ClientResponse { get; set; }

        public NotLicensedException(string message, ClientResponse response)
            : base(message)
        {
            this.ClientResponse = response;
        }

        public NotLicensedException(string message, ClientResponse response, Exception ex)
            : base(message, ex)
        {
            this.ClientResponse = response;
        }

        public NotLicensedException(string message)
            : base(message)
        {
        }

        public NotLicensedException(string message, Exception ex)
            : base(message, ex)
        {
        }
    }
}
