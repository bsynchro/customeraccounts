﻿using CustomerAccounts.Client.Entities;
using System;

namespace CustomerAccounts.Common.Exceptions
{
    public class WarningException : ApplicationException
    {
        public ClientResponse ClientResponse { get; set; }

        public WarningException(string message)
            : base(message)
        {
        }

        public WarningException(string message, ClientResponse response)
            : base(message)
        {
            this.ClientResponse = response;
        }


        public WarningException(string message, ClientResponse response, Exception ex)
            : base(message, ex)
        {
            this.ClientResponse = response;
        }

        public WarningException(string message, Exception exception)
            : base(message, exception)
        {
        }
    }

    public class NotCompletedException : ApplicationException
    {

        public ClientResponse ClientResponse { get; set; }

        public NotCompletedException(string message, ClientResponse response)
            : base(message)
        {
            this.ClientResponse = response;

        }

        public NotCompletedException(string message, ClientResponse response, Exception ex)
            : base(message, ex)
        {
            this.ClientResponse = response;

        }

        public NotCompletedException(string message)
            : base(message)
        {
        }

        public NotCompletedException(string message, Exception ex)
            : base(message, ex)
        {
        }
    }

}
