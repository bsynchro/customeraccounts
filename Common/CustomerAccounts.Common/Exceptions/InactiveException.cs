﻿using CustomerAccounts.Client.Entities;
using System;

namespace CustomerAccounts.Common.Exceptions
{

    public class InactiveException : ApplicationException
    {

        public ClientResponse ClientResponse { get; set; }

        public InactiveException(string message)
            : base(message)
        {
        }

        public InactiveException(string message, Exception ex)
            : base(message, ex)
        {
        }

        public InactiveException(string message, ClientResponse response)
            : base(message)
        {
            this.ClientResponse = response;

        }

        public InactiveException(string message, ClientResponse response, Exception ex)
            : base(message, ex)
        {
            this.ClientResponse = response;

        }

    }
}
