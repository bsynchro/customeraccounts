﻿using CustomerAccounts.Client.Entities;
using System;

namespace CustomerAccounts.Common.Exceptions
{
    public class NotValidException : ApplicationException
    {
        public ClientResponse ClientResponse { get; set; }

        public NotValidException(string message, ClientResponse response)
            : base(message)
        {
            this.ClientResponse = response;
        }

        public NotValidException(string message, ClientResponse response, Exception ex)
            : base(message, ex)
        {
            this.ClientResponse = response;
        }


        public NotValidException(string message)
            : base(message)
        {
        }


        public NotValidException(string message, Exception exception)
            : base(message, exception)
        {
        }
    }
}
