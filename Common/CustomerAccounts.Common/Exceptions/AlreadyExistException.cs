﻿using CustomerAccounts.Client.Entities;
using System;

namespace CustomerAccounts.Common.Exceptions
{
    public class AlreadyExistException : ApplicationException
    {
        public ClientResponse ClientResponse { get; set; }

        public AlreadyExistException(string message)
            : base(message)
        {
        }

        public AlreadyExistException(string message, ClientResponse response)
            : base(message)
        {
            this.ClientResponse = response;
        }


        public AlreadyExistException(string message, ClientResponse response, Exception ex)
            : base(message, ex)
        {
            this.ClientResponse = response;
        }

        public AlreadyExistException(string message, Exception exception)
            : base(message, exception)
        {
        }
    }
}
