﻿using CustomerAccounts.Client.Entities;
using System;

namespace CustomerAccounts.Common.Exceptions
{
    public class NotModifiedException : ApplicationException
    {
        public ClientResponse ClientResponse { get; set; }

        public NotModifiedException(string message)
            : base(message)
        {
        }

        public NotModifiedException(string message, ClientResponse response)
            : base(message)
        {
            this.ClientResponse = response;
        }

        public NotModifiedException(string message, Exception ex)
            : base(message, ex)
        {
        }

        public NotModifiedException(string message, ClientResponse response, Exception ex)
            : base(message, ex)
        {
            this.ClientResponse = response;
        }
    }
}
