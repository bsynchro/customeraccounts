﻿using CustomerAccounts.Client.Entities;
using System;

namespace CustomerAccounts.Common.Exceptions
{
    public class NotFoundException : ApplicationException
    {
        public ClientResponse ClientResponse { get; set; }

        public NotFoundException(string message)
            : base(message)
        {
        }

        public NotFoundException(string message, ClientResponse response)
            : base(message)
        {
            this.ClientResponse = response;
        }

        public NotFoundException(string message, ClientResponse response, Exception ex)
            : base(message, ex)
        {
            this.ClientResponse = response;
        }

        public NotFoundException(string message, Exception exception)
            : base(message, exception)
        {
        }
    }
}
