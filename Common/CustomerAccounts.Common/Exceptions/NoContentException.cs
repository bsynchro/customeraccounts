﻿using CustomerAccounts.Client.Entities;
using System;

namespace CustomerAccounts.Common.Exceptions
{
    public class NoContentException : ApplicationException
    {
        public ClientResponse ClientResponse { get; set; }

        public NoContentException(string message)
            : base(message)
        {
        }

        public NoContentException(string message, ClientResponse response)
            : base(message)
        {
            this.ClientResponse = response;
        }


        public NoContentException(string message, ClientResponse response, Exception ex)
            : base(message, ex)
        {
            this.ClientResponse = response;
        }

        public NoContentException(string message, Exception exception)
            : base(message, exception)
        {
        }
    }
}
