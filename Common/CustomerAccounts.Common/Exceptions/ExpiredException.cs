﻿using CustomerAccounts.Client.Entities;
using System;

namespace CustomerAccounts.Common.Exceptions
{
    public class ExpiredException : ApplicationException
    {
        public ClientResponse ClientResponse { get; set; }

        public ExpiredException(string message, ClientResponse response)
            : base(message)
        {
            this.ClientResponse = response;
        }

        public ExpiredException(string message, ClientResponse response, Exception ex)
            : base(message, ex)
        {
            this.ClientResponse = response;
        }


        public ExpiredException(string message)
            : base(message)
        {
        }


        public ExpiredException(string message, Exception exception)
            : base(message, exception)
        {
        }
    }
}
