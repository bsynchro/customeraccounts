﻿using CustomerAccounts.Client.Entities;
using CustomerAccounts.Common.Static;
using System;

namespace CustomerAccounts.Common.Extensions
{
    public static class Extensions
    {
        public static ClientResponse ClientResponse(this Exception source)
        {
            Error errorResponse = new Error(ErrorCodes.InternalServerError, source.Message, Messages.InternalServerErrorTitle);

            ClientResponse response = new ClientResponse()
            {
                Title = errorResponse.Title,
                Status = ErrorCodes.Failure,
                Data = new Error(
                       errorResponse.Code,
                       errorResponse.Message
                   ),
                ResponseCode = System.Net.HttpStatusCode.InternalServerError
            };

            return response;
        }
    }
}
