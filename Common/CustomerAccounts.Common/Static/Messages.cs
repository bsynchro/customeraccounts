﻿namespace CustomerAccounts.Common.Static
{
    public class Messages
    {

        public const string UserNotFound = "User not found, Create it first";
        public const string Success = "success";
        public const string Failed = "failed";
        public const string Failure = "failure";

        #region Titles

        public const string InvalidDataTitle = "Missing Information";
        public const string NotFoundTitle = "Not Found";
        public const string AlreadyExistTitle = "Entity Already Exists";
        public const string NoContentTitle = "No Content found";
        public const string WarningTitle = "Warning";
        public const string UnAuthorizedTitle = "Unauthorized operation";
        public const string InactiveTitle = "Inactive operation";
        public const string ExpiredTitle = "Expired operation";
        public const string NotIntegratedTitle = "Operartion Not Integrated";
        public const string IncompleteTitle = "Incomplete";
        public const string InternalServerErrorTitle = "Internal server error";
        public const string NotAuthenticatedTitle = "Not Authenticated";
        public const string NotLicensedTitle = "Not Licensed";
        public const string IntegrationErrorTitle = "Integration Error";
        public const string NotVerifiedTitle = "Not Verified";

        #endregion Titles

  
    }


}
