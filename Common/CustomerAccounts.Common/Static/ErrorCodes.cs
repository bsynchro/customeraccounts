﻿namespace CustomerAccounts.Common.Static
{
    public class ErrorCodes
    {
            public const int NotAuthenticated = 30;
            public const int StoreNotLicensed = 32;
            public const int BadRequestData = 34;
            public const int Expired = 35;
            public const int NotFound = 36;
            public const int InternalServerError = 38;
            public const int UnknownInternalError = 40;
            public const int IntegrationInternalError = 42;
            public const int NoContent = 44;


            public const int NotVerified = 46;
            public const int NotCompleted = 48;
            public const int AlreadyExist = 49;
            public const int NotConfigured = 50;
            public const int Inactive = 52;
            public const int SuccessCode = 0;
            public const string Success = "success";
            public const string Failure = "failure";
    }
}
